# JWT_Registration_API

# ![Техническое задание](img/technical_specification.jpg)

# Необходимые зависимости:
* docker-compose 
* psql (Для создания Бд)
* goose (Для работы с миграциями)
* make (Для исполнения команд Makefile)

# Запуск приложения

1. Запускаем контейнеры docker jwt_registration_api и db при помощи команды:
```
docker-compose up
```

2. После того, как контейнер db запущен, создаем базу данных, без миграций в контейнере db:
```
make create-db
```

3. Далее накатываем миграции в созданную БД командой:
```
make migrations-up
```
Теперь оба контейнеры должны запуститься и корректно работать.

# Коллекция запросов API в Postman
Кнопка для использования моей коллекции запросов в Postman
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/13406714-8962361f-ddcb-4a8e-b192-c794e1ca9260?action=collection%2Ffork&collection-url=entityId%3D13406714-8962361f-ddcb-4a8e-b192-c794e1ca9260%26entityType%3Dcollection%26workspaceId%3Da983e7b1-75a9-47c9-a5da-fc0d66dfecc9)

# Пример использования API
1. Регистрация пользователя **http://127.0.0.1:3012/register**  
   Тело запроса:
```
{
    "login": "my_login2",
    "email": "very_nice@yandex.ru",
    "password": "1234567",
    "phone_number": "89175382453"
}
```
   Результат: 
   ![Результат register](img/reg_res.png)

2. Вход пользователя **http://127.0.0.1:3012/login**  
   Тело запроса:  
```
{
    "login": "my_login_new",
    "password": "1234567"
}
```
Результат:  
  ![Результат login](img/login_res.png)


# Используемые технологии
* Чистая архитектура
* Бд PostgreSQL
* docker
* docker-compose
* Makefile для запуска скриптов
* goose для работы с миграциями БД
* Viper для работы с конфигом
* Logrus для логгирования
* JWT для создания токенов доступа
* bcrypt для хэширования пароля пользователя
* squirrel для генерации SQL-запросов
* golangci-lint статический анализатор кода (линтер)
* gitlab-ci.yml
  