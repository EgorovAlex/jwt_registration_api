package user

const (
	TableAuthUser     = "auth_user"
	ColumnsAuthUser   = "login, email, password, phone_number"
	SelectAllAuthUser = "id, login, email, password, phone_number"
)
