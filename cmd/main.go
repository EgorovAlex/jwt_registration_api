package main

import (
	"context"
	"fmt"
	"jwt_registration_api/internal/composites"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/julienschmidt/httprouter"
	"github.com/spf13/viper"

	"github.com/sirupsen/logrus"
)

const configPath = "config/app_conf.yaml"

func main() {
	logger := logrus.New()

	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		logger.Fatal(err)
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = viper.GetString("defaultPort")
	}

	dbComposite, err := composites.NewPostgresComposite(viper.GetString("db.url"))
	if err != nil {
		logger.Fatal(err)
	}

	signingJwtKey := viper.GetString("signingJwtKey")
	hoursOfJwtAction := viper.GetInt("hoursOfJwtToken")
	userComposite, err := composites.NewUserComposite(dbComposite, signingJwtKey, hoursOfJwtAction, logger)
	if err != nil {
		logger.Fatal(err)
	}

	router := httprouter.New()
	userComposite.Handler.RegisterRoute(router)

	srv := http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: router,
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	go func() {
		logger.Fatal(srv.ListenAndServe())
	}()

	<-ctx.Done()
	logger.Info("Graceful Shutdown")

	if err := srv.Shutdown(context.Background()); err != nil {
		logger.Fatalf("%v", err)
	}

	if err := dbComposite.Db.Close(); err != nil {
		logger.Fatalf("%v", err)
	}
}
